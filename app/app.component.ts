import {Component, ViewEncapsulation} from "@angular/core";
import {TranslateService} from "ng2-translate";

@Component({
    selector: 'my-app',
    encapsulation: ViewEncapsulation.None,
    template: '<router-outlet></router-outlet>',
    styles: [
        require('./app.scss'),
        require('../node_modules/font-awesome/scss/font-awesome.scss')
    ]
})

export class AppComponent {
    constructor(translate: TranslateService) {
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('us_US');

        // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use('us_US');
    }
}