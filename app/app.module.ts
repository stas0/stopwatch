import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from "./app.component";
import { routing } from "./app.routing";
import {NavigationComponent} from "./navigation/navigation.component";
import {MdInput} from "@angular2-material/input";
import {FormsModule, FormBuilder, ReactiveFormsModule} from "@angular/forms";
import {MdButton} from "@angular2-material/button";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {HttpModule, Http} from "@angular/http";
import {Ng2Webstorage} from 'ng2-webstorage';
import * as $ from "jquery";
import {TranslateModule, TranslateStaticLoader, TranslateLoader} from "ng2-translate";
import { FileSelectDirective } from 'ng2-file-upload';
import {HomeComponent} from "./home/home";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing,
        Ng2Webstorage,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (http: Http) => new TranslateStaticLoader(http, '/assets/i18n', '.json'),
            deps: [Http]
        })
    ],
    declarations: [
        MdInput,
        MdButton,
        AppComponent,
        FileSelectDirective,
        NavigationComponent,
        HomeComponent,
    ],
    entryComponents: [

    ],
    providers: [

    ],
    bootstrap: [AppComponent]
})

export class AppModule {}