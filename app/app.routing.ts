import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import {NavigationComponent} from "./navigation/navigation.component";
import {HomeComponent} from "./home/home";

const appRoutes: Routes = [
    {
        path: '',
        component: NavigationComponent,
        children: [
            {
                path: '',
                component: HomeComponent
            }
        ]
    }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);