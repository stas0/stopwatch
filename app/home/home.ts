import {Component} from "@angular/core";
import {LocalStorageService} from "ng2-webstorage";

declare var SegmentDisplay;

@Component({
    selector: 'home',
    template: require('./home.html'),
    styles: [
        require('./home.scss')
    ]
})
export class HomeComponent {
    private display: any;
    private startTime: number;
    private flashSeparator: boolean = true;
    private play: boolean = false;
    private pause: boolean = false;
    private pastTime: number = 0;
    private currStopList: number = 0;

    constructor(private localSt: LocalStorageService) {
        $(window).on('storage', (e) => {
            let key: string = e.key.split('|')[1];

            if (key == undefined) {
                return;
            }

            if (key == 'event-name') {
                let eventName: string = this.localSt.retrieve('event-name');
                let timerStopItemIndex: number;

                if (eventName.indexOf('remove-stop-timer-item_') == 0) {
                    timerStopItemIndex = parseInt(eventName.substring('remove-stop-timer-item_'.length));
                    eventName = 'remove-stop-timer-item_';
                } else if (eventName.indexOf('remove-stop-timer-item') == 0) {
                    timerStopItemIndex = parseInt(eventName.substring('remove-stop-timer-item'.length));
                    eventName = 'remove-stop-timer-item';
                }

                switch (eventName) {
                    case 'start-stopwatch':
                        this.startStopwatch();

                        break;

                    case 'pause-stopwatch':
                        this.pauseStopwatch();

                        break;

                    case 'remove-stopwatch':
                        this.resetStopwatch();

                        break;

                    case 'add-stop-timer':
                        this.addStopTimer();

                        break;

                    case 'add-stop-timer_':
                        this.addStopTimer();

                        break;

                    case 'remove-stop-timer-item':
                        this.removeStopTimerItemByIndex(timerStopItemIndex);

                        break;

                    case 'remove-stop-timer-item_':
                        this.removeStopTimerItemByIndex(timerStopItemIndex);

                        break;
                }
            }
        });
    }

    ngOnInit() {
        this.display = new SegmentDisplay("display");
        this.display.pattern = "##:##.##";
        this.display.cornerType = 2;
        this.display.displayType = 7;
        this.display.displayAngle = 0;
        this.display.digitHeight = 25;
        this.display.digitWidth = 14;
        this.display.digitDistance = 4.2;
        this.display.segmentWidth = 1.5;
        this.display.segmentDistance = 1;
        this.display.colorOn = "rgba(36, 214, 59, 1)";
        this.display.colorOff = "rgba(36, 214, 59, 0.3)";

        this.resetStopwatch();

        let pastTime: number = this.localSt.retrieve('past-time');
        let startTime: number = this.localSt.retrieve('start-time');
        let play: boolean = this.localSt.retrieve('play');
        let pause: boolean = this.localSt.retrieve('pause');
        let stopList: any = this.localSt.retrieve('stop-list');

        if (stopList != null) {
            this.clearStopTimerList();
            stopList = stopList.split(';');
            stopList.pop();

            for (let item of stopList) {
                this.addStopTimer(item);
            }
        }

        if (pastTime != null && startTime != 0) {
            this.pastTime = pastTime;
            this.startTime = startTime;
            this.play = play;
            this.pause = !play;

            if (pastTime != 0 && startTime != 0) {
                if (!play) {
                    this.startTime = Date.now() - pastTime;
                }

                this.display.setValue(this.getDisplayValue());
            }

            if (play) {
                this.animate();
                this.animateFlashSeparator();
            }
        }
    }

    switchStopwatch() {
        if (this.play) {
            this.pauseStopwatch();
        } else {
            this.startStopwatch();
        }
    }

    resetStopwatch() {
        this.play = false;
        this.pause = false;
        this.startTime = 0;
        this.pastTime = 0;
        this.display.setValue('00:00.00');
        this.clearStopTimerList();
    }

    removeStopwatch() {
        this.resetStopwatch();

        this.saveToLocalStorage('past-time', this.pastTime);
        this.saveToLocalStorage('start-time', this.startTime);
        this.saveToLocalStorage('play', this.play);
        this.saveToLocalStorage('stop-list', '');
        this.saveToLocalStorage('event-name', 'remove-stopwatch');
    }

    startStopwatch() {
        this.play = true;

        if (this.pause) {
            this.pause = false;
            this.startTime = Date.now() - this.pastTime;
        } else {
            this.startTime = Date.now();
        }

        this.saveToLocalStorage('start-time', this.startTime);
        this.saveToLocalStorage('play', this.play);
        this.saveToLocalStorage('event-name', 'start-stopwatch');

        this.animate();
        this.animateFlashSeparator();
    }

    pauseStopwatch() {
        this.play = false;
        this.pause = true;

        this.pastTime = Date.now() - this.startTime;

        this.saveToLocalStorage('past-time', this.pastTime);
        this.saveToLocalStorage('play', this.play);
        this.saveToLocalStorage('pause', this.pause);
        this.saveToLocalStorage('event-name', 'pause-stopwatch');
    }

    addStopTimer(time?: string) {
        if (this.play && this.pause == false || time != null) {
            if (time == null) {
                if (document.hasFocus()) {
                    if (this.localSt.retrieve('event-name') === 'add-stop-timer') {
                        this.localSt.store('event-name', 'add-stop-timer_');
                    } else {
                        this.localSt.store('event-name', 'add-stop-timer');
                    }
                }
            }

            this.createStopTimer(time);
        }
    }

    animateFlashSeparator() {
        if (!this.play) {
            return;
        }

        this.flashSeparator = !this.flashSeparator;

        window.setTimeout(() => {
            this.animateFlashSeparator();
        }, 500);
    }

    animate() {
        if (!this.play) {
            return;
        }

        this.display.setValue(this.getDisplayValue());

        window.setTimeout(() => {
            this.animate();
        }, 100);
    }

    getDisplayValue(): string {
        let time = new Date(Date.now() - this.startTime);
        let minutes = time.getMinutes();
        let seconds = time.getSeconds();
        let milliseconds = time.getMilliseconds().toString();

        if (milliseconds.length > 2) {
            milliseconds = milliseconds.substring(0, 2);
        }

        let value;

        if (this.flashSeparator) {
            value = ((minutes < 10) ? '0' : '') + minutes
                + ':' + ((seconds < 10) ? '0' : '') + seconds
                + '.' + milliseconds;
        } else {
            value = ((minutes < 10) ? '0' : '') + minutes
                + '_' + ((seconds < 10) ? '0' : '') + seconds
                + '|' + milliseconds;
        }

        return value;
    }

    createStopTimer(time?: string) {
        this.currStopList++;
        let template: any = $('#stop-list-template').clone();
        template.removeAttr('id');
        template.find('.display').attr('id', `stop-list-${this.currStopList}`);
        template.find('.remove > div').click((e) => {
            let parent = $(e.target).parent().parent();
            let parentIndex: number = parent.index();
            parent.remove();

            let stopList: any = this.localSt.retrieve('stop-list');
            stopList = stopList.split(';').reverse();
            stopList.shift();
            stopList.splice(parentIndex, 1);
            stopList = stopList.reverse();

            if (stopList.length != 0) {
                stopList = stopList.join(';') + ';';
            } else {
                stopList = '';
            }

            this.localSt.store('stop-list', `${stopList}`);

            if (document.hasFocus()) {
                let eventName1: string = `remove-stop-timer-item${parentIndex}`;
                let eventName2: string = `remove-stop-timer-item_${parentIndex}`;

                if (this.localSt.retrieve('event-name') == eventName1) {
                    this.localSt.store('event-name', eventName2);
                } else {
                    this.localSt.store('event-name', eventName1);
                }
            }
        });
        $('.stop-list').prepend(template);

        let display = new SegmentDisplay(`stop-list-${this.currStopList}`);
        display.pattern = "##:##.##";
        display.cornerType = 2;
        display.displayType = 7;
        display.displayAngle = 0;
        display.digitHeight = 12.5;
        display.digitWidth = 7;
        display.digitDistance = 2.1;
        display.segmentWidth = 0.75;
        display.segmentDistance = 0.5;
        display.colorOn = "rgba(255, 255, 255, 1)";
        display.colorOff = "rgba(255, 255, 255, 0.3)";

        if (time == null) {
            time = this.getDisplayValue();
            time = time.replace('_', ':');
            time = time.replace('|', '.');

            let stopList: string = this.localSt.retrieve('stop-list');

            if (stopList == null) {
                stopList = '';
            }

            this.saveToLocalStorage('stop-list', `${stopList}${time};`);
        }

        display.setValue(time);
    }

    clearStopTimerList() {
        $('.stop-list > div').remove();
    }

    removeStopTimerItemByIndex(index: number) {
        $(`.stop-list > div:eq(${index})`).remove();
    }

    saveToLocalStorage(name: string, value: any) {
        if (document.hasFocus()) {
            this.localSt.store(name, value);
        }
    }
}