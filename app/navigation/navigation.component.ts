import {Component} from "@angular/core";

@Component({
    selector: "navigation",
    template: require("./navigation-component.html"),
    styles: [
        require("./navigation-component.scss")
    ]
})

export class NavigationComponent {
}