var path = require('path');
var webpack = require("webpack");
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
	entry: {
		app: 'main.ts'
	},
	output: {
		path: __dirname,
		filename: './dist/app.js'
	},
	resolve: {
		extensions: ['', '.js', '.ts']
	},
	module: {
		loaders: [
			{
				test: /\.ts/,
				loaders: ['ts-loader'],
				exclude: /node_modules/
			},
			{
				test: /\.html$/,
				loader: "html"
			},
			{
				test: /\.scss$/,
				loaders: ["raw", "sass"]
			},
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            }
		]
	},
    plugins: [
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            jquery: 'jquery'
        }),
        new CopyWebpackPlugin([
			{
                from: 'app/assets/i18n', to: 'assets/i18n'
			},
            {
                from: 'app/assets/images', to: 'assets/images'
            }
		])
    ],
	resolve: {
		root: path.resolve('app'),
		extensions: ['', '.js', '.ts']
	},
	devServer: {
		host: 'localhost',
		port: 3000,
        historyApiFallback: true
	},
	watch: true,
	cache: false
}